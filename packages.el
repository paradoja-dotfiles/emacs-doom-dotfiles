;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; TODO Remove
;; (unpin! org-roam)
(package! org-roam-ui)

(package! org-super-agenda)

(package! org-special-block-extras)

(package! org-ql)

(package! anki-editor
  :recipe (:host github :repo "anki-editor/anki-editor"))
(package! ankiorg
  :recipe (:host github :repo "orgtre/ankiorg"))

(package! org-transclusion)

(package! lox-mode)                     ; from https://craftinginterpreters.com/

(package! graphviz-dot-mode)

(package! verb)

(package! exercism-modern
  :recipe (:files (:defaults "icons")
           :host github :repo "elken/exercism-modern"))

(package! projectile-phoenix
  :recipe (:host github :repo "Auralcat/projectile-phoenix"))
(package! apprentice
  :recipe (:host github :repo "Sasanidas/Apprentice"))

(package! mermaid-mode)
(package! ob-mermaid)

(package! sql-indent)

(package! gptel)

;; TODO Remove - https://github.com/org-roam/org-roam/issues/2485
;; (package! emacsql :pin "491105a01f58bf0b346cbc0254766c6800b229a2")

;; HACK For elixir ~H and ~L templates
(package! mmm-mode)
(package! fence-edit
  :recipe (:host github :repo "aaronbieber/fence-edit.el"))

;; TODO Added to make sure apheleia has ruff
(unpin! apheleia)

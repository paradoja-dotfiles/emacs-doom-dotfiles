;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(require 'f)

;; (setq flymake-allowed-file-name-masks '())
(load! "utils/paradoja.el")

(setq user-full-name "Abby Henríquez Tejera"
      user-mail-address "abby@blulaktuko.net")


;; Visual changes
(setq doom-font (font-spec :family "Iosevka Term Slab" :size 14.0)
      doom-variable-pitch-font (font-spec :family "Iosevka Slab"))

(add-to-list 'initial-frame-alist '(fullscreen . maximized))

(defvar +paradoja-themes '(doom-one . doom-one-light))
(setq doom-theme (car +paradoja-themes))

(setq display-line-numbers-type nil)

(setq display-time-24hr-format t)
(map! :desc "Display time" :map doom-leader-toggle-map
      "H" #'display-time-mode)

(defun +paradoja-big-window-switching-fonts (&optional theme no-confirm no-enable)
  (eval
   `(custom-set-faces!
      '(aw-leading-char-face
        :foreground ,(doom-color 'bg) :background ,(doom-color 'fg)
        :weight bold
        :height ,(font-get doom-font :size)
        :box (:line-width ,(* 2 doom-font-increment) :color ,(doom-color 'highlight))))))

(advice-add #'load-theme :after #'+paradoja-big-window-switching-fonts)

(remove-hook 'doom-first-file-hook #'centaur-tabs-mode) ; don't show tabs by default
(map! :desc "Toggle tabs" :map doom-leader-toggle-map
      "t" #'centaur-tabs-mode)

(remove-hook 'prog-mode-hook #'highlight-indent-guides-mode)
(remove-hook 'text-mode-hook #'highlight-indent-guides-mode)
(remove-hook 'conf-mode-hook #'highlight-indent-guides-mode)

;; Ivy, general evil, and standard behaviour
(map! :nvi "C-x C-c" (cmd! (message "C-x C-c"))) ; avoid exiting by mistake
(setq disabled-command-function nil)     ; no disabled commands
;;; TAB n for new workspace is superannoying for me
(map! :leader
      :desc "Next workspace" "TAB n" #'+workspace:switch-next
      :desc "Prev workspace" "TAB p" #'+workspace:switch-previous
      :desc "New workspace" "TAB C-t" #'+workspace/new)

(setq enable-local-variables t)

(after! ranger
  ;; Don't want ranger to fully override dired
  (setq ranger-override-dired nil
        ranger-override-dired-mode nil))

(progn
  (setq iedit-toggle-key-default "M-¡") ; iedit is very rude
  (map! :nv "M-¡" #'iedit-mode))

(add-hook 'after-save-hook               ; TODO: per major mode?
          #'executable-make-buffer-file-executable-if-script-p)
(add-hook 'before-save-hook #'delete-trailing-whitespace)

(after! evil
  (setq evil-want-fine-undo t)

  ;; C-t is "beach first line property" keybinding, and we use it for something
  ;; more useful
  (map! :nvi "C-t"
        (cmd! ()
              (message "Toggling evil-want-minibuffer")
              (setq evil-want-minibuffer (not evil-want-minibuffer)))))

(defun +paradoja-toggle-theme ()
  "Toggle current themes."
  (interactive)
  (let ((get-next (if (equal (car custom-enabled-themes)
                             (car +paradoja-themes))
                      #'cdr
                    #'car)))
    (load-theme (funcall get-next +paradoja-themes))))

(map! :desc "Toggle theme" :map doom-leader-toggle-map
      "T" #'+paradoja-toggle-theme
      :desc "Toggle ligatures" :map doom-leader-toggle-map
      "L" #'prettify-symbols-mode)
(map! :n "gsl" #'avy-goto-line)
(map! :after evil :i "C-d" #'+snippets/delete-forward-char-or-field)
(map! :leader :desc "Dashboard open" "o<" #'+doom-dashboard/open)
(map! :after multiple-cursors           ; iedit decides to do its own thing
      :map iedit-occurrence-keymap-default :nv
      "M-D"
      #'evil-multiedit-match-symbol-and-prev)
(map! :after evil :ig (kbd "S-<dead-diaeresis> _") (lambda () (interactive) (insert "¨")))

;;; Dictionary
(after! ispell
  (ispell-change-dictionary "en_GB" t)
  (ispell-hunspell-add-multi-dic "es_ES,en_GB")
  (add-to-list 'ispell-dictionary-alist
               '("en_GB" "[[:alpha:]]" "[^[:alpha:]]" "[0-9']" t ("-d" "en_GB") nil utf-8)))

;; Project related
(map! :after projectile
      :map doom-leader-project-map
      "m" #'+make/run
      "M" #'+make/run-last)

(map! :after treemacs
      :n "C-c -" #'treemacs-select-window)

;; Org
(load! "org/config.el")
(map! :leader
      ;; Top level key bindings change - that's why this is here.
      ;;
      ;; We exchange SPC x with SPC X, as capturing is more common for me and I
      ;; mistakenly open the scratch buffer too much. Probably doesn't need
      ;; `:after org`, but maybe it makes intentions clear.
      :nv "x" #'org-capture
      :nv "X" #'doom/open-scratch-buffer)

;; Programming, LSP
(setq +lsp-company-backends '(:separate company-yasnippet company-capf)) ; yasnippets first

(after! lsp-ui-doc
  (setq lsp-ui-doc-enable t ; already set to t by default; left just to make sure
        lsp-ui-sideline-show-hover t))

(after! cc
  (add-hook 'c++-mode (lambda ()
                        (setq dap-auto-configure-mode t)
                        (require 'dap-cpptools)))
  )
(after! elixir-mode
  (add-hook 'elixir-mode-hook
            (lambda () (progn ;; (setq +format-with :none)
                         ;; (add-hook 'before-save-hook #'elixir-format t t)
                         (projectile-phoenix-mode 1)
                         (apprentice-mode)
                                        ;(apheleia-mode -1)
			 )))
  ;; HACK Until Polymode works (or an alternative that is not only font-lock)
  (use-package! mmm-mode)
  (require 'mmm-mode)
  (require 'web-mode)
  (setq mmm-global-mode 'maybe)
  (setq mmm-parse-when-idle 't)
  (setq mmm-set-file-name-for-modes '(web-mode))
  (custom-set-faces '(mmm-default-submode-face ((t (:background nil)))))
  (let ((class 'elixir-eex)
        (submode 'web-mode)
        (front (rx line-start (* space) (or "~L" "~H") (= 3 (char "\"'")) line-end))
        (back  (rx line-start (* space) (= 3 (char "\"'")) line-end)))
    (mmm-add-classes (list (list class :submode submode :front front :back back)))
    (mmm-add-mode-ext-class 'elixir-mode nil class))

  ;; (define-advice web-mode-guess-engine-and-content-type (:around (f &rest r) guess-engine-by-extension)
  ;;   (if (and buffer-file-name (equal "ex" (file-name-extension buffer-file-name)))
  ;;       (progn (setq web-mode-content-type "html")
  ;;              (setq web-mode-engine "elixir")
  ;;              (web-mode-on-engine-setted))
  ;;     (apply f r)))

  (defun +paradoja/elixir-open-documentation (r-begin r-end)
    (interactive "r")
    (let ((symbol (if (use-region-p)
                      (buffer-substring r-begin r-end)
                    (symbol-at-point))))
      (if symbol
          (browse-url (format "https://hexdocs.pm/elixir/search.html?q=%s" symbol))
        (message "Elixir open doc: found no symbol"))))

  (require 'fence-edit)
  (add-to-list 'fence-edit-blocks
               `(,(rx line-start (* space) (or "~L" "~H") (= 3 (char "\"'")) line-end)
                 ,(rx line-start (* space) (= 3 (char "\"'")) line-end)
                 web))

  (map! :localleader :map web-mode-map
        :desc "Edit Web template block" "w" #'fence-edit-dwim)
  (map! :localleader :map elixir-mode-map
        :desc "Edit Web template block" "w" #'fence-edit-dwim
        (:prefix ("d" . "Doc")
         :desc "Main elixir documentation" "m" #'elixir-mode-open-docs-stable
         :desc "Doc at point if present" "d" #'+paradoja/elixir-open-documentation)
        (:prefix ("p" . "Projectile Phoenix find...")
         :desc "test" "t" #'projectile-phoenix-find-test
         :desc "view" "v" #'projectile-phoenix-find-view
         :desc "router" "r" #'projectile-phoenix-find-router
         :desc "worker" "w" #'projectile-phoenix-find-worker
         :desc "mix task" "k" #'projectile-phoenix-find-mix-task
         :desc "template" "T" #'projectile-phoenix-find-template
         :desc "migration" "M" #'projectile-phoenix-find-migration
         :desc "seed file" "s" #'projectile-phoenix-find-seed-file
         :desc "controller" "c" #'projectile-phoenix-find-controller
         :desc "module in context" "m" #'projectile-phoenix-find-module-in-context)
        (:prefix ("a" . "Apprentice")
         :desc "refcard" "r" #'apprentice-refcard)
        (:prefix ("m" . "Mix")
         :desc "compile" "c" (cmd! (apprentice-mix-compile ""))
         :desc "Compile (with opts)" "C" #'apprentice-mix-compile
         :desc "run" "r" (cmd! #'apprentice-mix-execute (list "credo" "--strict"))))

  (use-package! apprentice))

(after! scala                           ; scala 3 support
  ;; https://sideshowcoder.com/2021/12/30/new-scala-3-syntax-in-emacs/
  (defun is-scala3-project ()
    "Check if the current project is using scala3.

Loads the build.sbt file for the project and serach for the scalaVersion."
    (projectile-with-default-dir (projectile-project-root)
      (when (file-exists-p "build.sbt")
        (with-temp-buffer
          (insert-file-contents "build.sbt")
          (search-forward "scalaVersion := \"3" nil t)))))

  (defun with-disable-for-scala3 (orig-scala-mode-map:add-self-insert-hooks &rest arguments)
    "When using scala3 skip adding indention hooks."
    (unless (is-scala3-project)
      (apply orig-scala-mode-map:add-self-insert-hooks arguments)))

  (advice-add #'scala-mode-map:add-self-insert-hooks :around #'with-disable-for-scala3)

  (defun disable-scala-indent ()
    "In scala 3 indent line does not work as expected due to whitespace grammar."
    (when (is-scala3-project)
      (setq indent-line-function 'indent-relative-maybe)))

  (add-hook 'scala-mode-hook #'disable-scala-indent))

(after! web-mode
  (setq web-mode-markup-indent-offset 2
        web-mode-code-indent-offset 2))

;; flycheck checkers form a chain, and there is one lsp checker, so chaining
;; after it is complicated. There is a fix in progress (see link), but meanwhile
;; this should work.
;;
;; https://github.com/flycheck/flycheck/issues/1762
(defvar-local +flycheck-local-checkers nil)
(after! syntax
  (defun +flycheck-checker-get (fn checker property)
    (or (alist-get property (alist-get checker +flycheck-local-checkers))
        (funcall fn checker property)))
  (advice-add 'flycheck-checker-get :around '+flycheck-checker-get)

  (add-hook 'sh-mode-hook
            (lambda ()
              (setq +flycheck-local-checkers '((lsp . ((next-checkers . (sh-shellcheck)))))))))

(use-package! exercism-modern
  :commands (exercism-modern-jump exercism-modern-view-tracks))

;; Python
;; TODO FIX https://github.com/doomemacs/doomemacs/issues/6317
(after! poetry
  (remove-hook 'python-mode-hook #'poetry-tracking-mode)
  (add-hook 'python-mode-hook 'poetry-track-virtualenv))

(after! (python apheleia)
  (setf (alist-get 'python-mode apheleia-mode-alist)
        '(ruff-isort ruff))
  (setf (alist-get 'python-ts-mode apheleia-mode-alist)
        '(ruff-isort ruff)))

;; https://mclare.blog/posts/using-uv-in-emacs/
(defun uv-activate ()
  "Activate Python environment managed by uv based on current project directory.
Looks for .venv directory in project root and activates the Python interpreter."
  (interactive)
  (let* ((project-root (project-root (project-current t)))
         (venv-path (expand-file-name ".venv" project-root))
         (python-path (expand-file-name
                       (if (eq system-type 'windows-nt)
                           "Scripts/python.exe"
                         "bin/python")
                       venv-path)))
    (if (file-exists-p python-path)
        (progn
          ;; Set Python interpreter path
          (setq python-shell-interpreter python-path)

          ;; Update exec-path to include the venv's bin directory
          (let ((venv-bin-dir (file-name-directory python-path)))
            (setq exec-path (cons venv-bin-dir
                                  (remove venv-bin-dir exec-path))))

          ;; Update PATH environment variable
          (setenv "PATH" (concat (file-name-directory python-path)
                                 path-separator
                                 (getenv "PATH")))

          ;; Update VIRTUAL_ENV environment variable
          (setenv "VIRTUAL_ENV" venv-path)

          ;; Remove PYTHONHOME if it exists
          (setenv "PYTHONHOME" nil)

          (message "Activated UV Python environment at %s" venv-path))
      (error "No UV Python environment found in %s" project-root))))

;;; Add lsp for Nix
(after! lsp
  (add-to-list 'lsp-language-id-configuration '(nix-mode . "nix"))
  (lsp-register-client
   (make-lsp-client :new-connection (lsp-stdio-connection '("rnix-lsp"))
                    :major-modes '(nix-mode)
                    :server-id 'nix)))
(after! nix-mode
  (add-hook 'nix-mode-hook #'lsp))

;; Emacs everywhere
(after! (emacs-everywhere org)
  (setq emacs-everywhere-major-mode-function #'org-mode
        emacs-everywhere-frame-name-format "Edit ∷ %s — %s"))

;; Other formats
(after! grip-mode
  (let ((credential (auth-source-user-and-password "api.github.com")))
    (setq grip-github-user (car credential)
          grip-github-password (cadr credential)))
  (setq grip-preview-use-webkit nil))

;; Email
(after! email
  (if (f-exists? "local/mail.el")
      (load! "local/mail.el")))
;; IRC
;; For some reason Circe comes without list
(defun circe-command-LIST (_)
  (irc-send-command (circe-server-process) "LIST"))

;; Graphic modes
(use-package! graphviz-dot-mode)

;; Work
(load! "utils/work.el")

;; TODO Fix projectile / skeleton
(defun projectile-skel-dir-locals (&optional str arg)
  "Insert a .dir-locals.el template."
  (interactive "*P
P")
  ;; this call was previously wrapped in atom-change-group
  (skeleton-proxy-new
   '(nil "((nil . ("
     (""
      '(projectile-skel-variable-cons)
      n)
     resume: ")))")
   str arg))

;; Term
(setq-default vterm-shell (executable-find "fish"))
(setq-default explicit-shell-file-name (executable-find "fish"))

;; hledger
(after! ledger
  (add-to-list 'auto-mode-alist '("\\.\\(h?ledger\\|journal\\|j\\)$" . ledger-mode))
  (setq ledger-binary-path (executable-find "ledger"))
  (setq ledger-mode-should-check-version nil)
  (setq ledger-report-links-in-register nil)
  (setq ledger-report-auto-width nil)
  (setq ledger-report-native-highlighting-arguments '("--color=always"))
  (setq ledger-highlight-xact-under-point nil)
  (require 'ledger-mode)                ; TODO add next line as a hook?
  (setq ledger-default-date-format ledger-iso-date-format))

;; gptel
(use-package! gptel
  :config
  ;; (setq! gptel-api-key "your key")
  )

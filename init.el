;;; init.el -*- lexical-binding: t; -*-
;; This file controls what Doom modules are enabled and what order they load
;; in. Remember to run 'doom sync' after modifying it!

;; NOTE Press 'SPC h d h' (or 'C-h d h' for non-vim users) to access Doom's
;;      documentation. There you'll find a "Module Index" link where you'll find
;;      a comprehensive list of Doom's modules and what flags they support.

;; NOTE Move your cursor over a module's name (or its flags) and press 'K' (or
;;      'C-c c k' for non-vim users) to view its documentation. This works on
;;      flags as well (those symbols that start with a plus).
;;
;;      Alternatively, press 'gd' (or 'C-c c d') on a module to browse its
;;      directory (for easy access to its source code).

;; This has to load early
(setq evil-respect-visual-line-mode t) ;; sane j and k behavior

(doom! :local                           ; local modules
       paradoja                         ; personal module

       :input
       ;;bidi
       ;;chinese
       ;;japanese
       ;;layout            ; auie,ctsrnm is the superior home row

       :completion
       ;; (company
       ;;  +childframe)
       (corfu
        +orderless
        +icons
        +dabbrev)
       ;; (ivy
       ;;  +fuzzy
       ;;  +prescient
       ;;  +icons)
       (vertico
        ;; +childframe ;; not really useful for Vertico
        +icons)

       :ui
       ;;deft              ; notational velocity for Emacs
       doom                      ; what makes DOOM look the way it does
       doom-dashboard            ; a nifty splash screen for Emacs
       doom-quit                 ; DOOM quit-message prompts when you quit Emacs
       (emoji
        +unicode)
       hl-todo                ; highlight TODO/FIXME/NOTE/DEPRECATED/HACK/REVIEW
       indent-guides          ; highlighted indent columns
       (ligatures)
       ;;minimap
       modeline
       nav-flash                     ; blink cursor line after big motions
       neotree                       ; XXX test
       ophints                       ; highlight the region an operation acts on
       (popup
        +defaults)                ; tame sudden yet inevitable temporary windows
       tabs
       treemacs
       unicode
       (vc-gutter
        +pretty)
       vi-tilde-fringe                  ; fringe tildes to mark beyond EOB
       window-select
       workspaces             ; tab emulation, persistence & separate workspaces
       zen                    ; distraction-free coding or writing

       :editor
       (evil
        +everywhere)
       file-templates
       fold                             ; (nigh) universal code folding
       (format
        ;; +lsp ;; TODO fails on python?
        +onsave)                   ; automated prettiness
       ;;god                         ; Deprecated
       lispy                       ; vim for lisp, for people who don't like vim
       multiple-cursors
       ;;objed
       ;;parinfer          ; turn lisp into python, sort of
       ;;rotate-text       ; cycle region at point between text candidates
       snippets
       ;;word-wrap         ; soft wrapping with language-aware indent

       :emacs
       (dired
        +icons
        +ranger)
       electric
       eww
       (ibuffer
        +icons)
       undo
       vc

       :term
       eshell
       ;;shell
       ;;term
       vterm

       :checkers
       (syntax
        +flymake)
       (spell
        +flyspell
        +hunspell
        +everywhere)
       ;;grammar           ; tasing grammar mistake every you make

       :tools
       ;;ansible
       biblio
       ;;collab
       (debugger
        +lsp)
       direnv
       docker
       ;;editorconfig      ; let someone else argue about tabs vs spaces
       ein                              ; tame Jupyter notebooks with emacs
       (eval +overlay)                  ; run code, run (also, repls)
       (lookup                        ; navigate your code and its documentation
        +dictionary
        offline)
       (lsp
        +peek)
       (magit
        +forge)
       make                             ; run make tasks from Emacs
       pass                             ; password manager for nerds
       pdf                              ; pdf enhancements
       ;;prodigy           ; FIXME managing external services & code builders
       ;;terraform         ; infrastructure as code
       ;;tmux              ; an API for interacting with tmux
       tree-sitter
       ;;upload            ; map local to remote projects via ssh/ftp

       :os
       (:if (featurep :system 'macos) macos)  ; improve compatibility with macOS
       (tty +osc)

       :lang
       ;;agda
       (beancount
        +lsp)
       (cc
        +lsp
        +tree-sitter)
       ;;clojure
       common-lisp
       ;;coq
       ;;crystal
       (csharp
        +lsp
        +dotnet
        +tree-sitter)
       data                             ; config/data formats
       ;;(dart +flutter)
       dhall
       (elixir
        +lsp
        +tree-sitter)
       elm
       emacs-lisp
       erlang
       ;;ess
       ;;factor
       ;;faust             ; dsp, but you get to keep your soul
       ;;fortran
       (fsharp
        +lsp)
       ;;fstar             ; (dependent) types and (monadic) effects and Z3
       ;;gdscript          ; the language you waited for
       (go
        +lsp
        +tree-sitter)
       (graphql
        +lsp)
       (haskell
        +lsp)
       hy
       ;;idris
       (json
        +lsp
        +tree-sitter)
       (java
        +lsp)
       (javascript
        +lsp
        +tree-sitter)
       ;;julia
       (kotlin
        +lsp)
       (latex
        +cdlatex
        +fold
        +lsp)
       ;;lean
       ;;factor
       ledger
       (lua
        +lsp)
       (markdown
        +grip)
       ;;nim
       (nix
        +tree-sitter
        +lsp)
       ;;ocaml
       (org
        ;;+brain
        +dragndrop
        ;;+gnuplot
        +hugo
        ;;+journal
        +noter
        +pandoc
        +pomodoro
        +present
        +pretty
        +roam2)
       (php
        ;; +lsp
        +tree-sitter)
       plantuml
       graphviz
       (purescript
        +lsp)
       (python
        +lsp
        +pyright
        +poetry
        +tree-sitter)
       ;;qt
       ;;(racket +lsp +xp)
       ;; raku
       rest
       ;;rst               ; ReST in peace
       (ruby
        +lsp
        +rails
        +tree-sitter)
       (rust
        +lsp)
       (scala
        +lsp
        +tree-sitter)
       ;;(scheme +guile)
       (sh
        +fish
        +lsp
        +tree-sitter)
       ;;sml
       ;;solidity
       ;;swift
       ;;terra             ; Earth and Moon in alignment for performance.
       (web
        +tree-sitter)
       yaml
       ;;zig

       :email
       (mu4e
        +gmail
        +org)
       ;;notmuch
       ;;(wanderlust +gmail)

       :app
       ;;calendar
       ;;emms
       everywhere
       irc
       ;;(rss +org)

       :config
       ;;literate
       (default +bindings +smartparens))

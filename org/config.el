;;; org/config.el -*- lexical-binding: t; -*-
(require 'f)
(require 'paradoja)

(after! org
  (setq! org-directory "~/org/"
         org-log-into-drawer "LOGBOOK")

  (setq! org-cycle-emulate-tab nil)

  (setq! org-pretty-entities t)

  (setq! org-startup-with-inline-images nil
         org-startup-with-latex-preview nil
         org-preview-latex-default-process 'dvisvgm)

  (setq! org-appear-trigger 'manual
         org-appear-autoemphasis t
         org-appear-autolinks t
         org-appear-autoentities t
         org-appear-autokeywords t
         org-appear-autosubmarkers t)
  (add-hook! 'org-mode-hook
             #'org-appear-mode
             (add-hook 'evil-insert-state-entry-hook
                       #'org-appear-manual-start
                       nil
                       t)
             (add-hook 'evil-insert-state-exit-hook
                       #'org-appear-manual-stop
                       nil
                       t))

  (map! :localleader :map org-mode-map
        :desc "Add note to properties" :nvi "-" #'org-add-note)
  (map! :map org-mode-map
        :desc "YAS next" :i "C-S-<iso-lefttab>" #'yas-next-field-or-maybe-expand)
  (map! :map org-mode-map
        :desc "Add nbsp" :i "C-c SPC" (cmd! (insert "\\nbsp{}"))
        :desc "Add `" :i "C-c t" (cmd! (insert "`"))
        :desc "Add ´" :i "C-c T" (cmd! (insert "´")))

  ;; org-id all the way
  (add-to-list 'org-modules 'org-id 'append)
  (setq org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id)

  (setq! org-todo-keywords
         '((sequence "TODO(t)" "HOLD(h@/!)" "|" "DONE(d@)" "KILL(k@)")
           (sequence "TALK(a!)" "|" "DISC(c!)" "DISX(x@)" "IGNR(i@)")
           (sequence "[ ](T)" "[-](S)" "[?](W)" "|" "[X](D)")
           (sequence "|" "OKAY(o)" "YES(y)" "NO(n)"))))

(map! :map doom-leader-toggle-map
      (:prefix ("o" "Org toggles")
       :desc "Pretty entities" "p" #'org-toggle-pretty-entities))

(after! (org evil)
  (setq! org-startup-folded 'show2levels))

(use-package! org-ql
  :after org)

(defun +paradoja/append-to-tag-list (original-list group-tag-to-append)
  "Returns a copy of the original org tag list (normally
`org-tag-persistent-alist') appended with a list of
`:startgrouptag' (list of) tags, possibly with nested group tags
itself. If there's a group tag with the same starting tag, then
it will be fully replaced with the one passed. If it's the same,
then the list returned will be identical (it's thus, idempotent).
If there's no such group, it will be added to the end.

To idempotently update `org-tag-persistent-alist' with new tag
groups* (and not get crazier with trees than needed),

1. We get the list until the \"work\" tag is found, and remove
the :startgrouptag - so we have the previous part of the list.

2. We find the matching :endgrouptag that would correspond to the
end of the grouping, taking into account that there may be inner
group tags; what's after this is the later part of the list.

3. We append the work tags with the hierarchy to the previous and
later parts.

This does mean we reconstruct almost the whole list, but it
should be fast enough unless there's lots of tags. It also
assumes there's only one appearance of the searched tag (or well,
we only do changes to the first), and that it's in a group tag.


We do it for now in a function with two recursive helper
functions, given the complexity of the requirement, before maybe
someday refactoring it.

(*) to add non-taggroup tags, simply use add-to-list ;) . This
function is to be used in project-local (or work) configurations.

Example usage:

#+BEGIN_SRC elisp
(setq! org-local-tags
       '((:startgrouptag)
         (\"work\")
         (:grouptags)
         (:startgrouptag)
         (\"project1\")
         (:grouptags)
         (\"p1-team1\") (\"p1-team2\")
         (\"p1-contractors\") (\"p1-else\")
         (\"p1-hiring\")
         (:endgrouptag)
         (\"project2\")
         (:endgrouptag))

       org-tag-persistent-alist
       (+paradoja/append-to-tag-list org-tag-persistent-alist
                                     org-local-tags))
#+END_SRC
"
  (let ((first-tag-key (caadr group-tag-to-append)))
    (cl-labels ((iterate-pre
                  (current-list prev-is-startgrouptag acc-list)
                  (let* ((current-sublist (car current-list))
                         (current-head (car current-sublist)))
                    (if (or (null current-list)
                            (and prev-is-startgrouptag
                                 (string= current-head first-tag-key)))
                        (-concat (nreverse acc-list) (iterate-in current-list 1))
                      (iterate-pre (cdr current-list)
                                   (eq current-head :startgrouptag)
                                   (cons current-sublist acc-list)))))
                (iterate-in
                  (current-list depth)
                  (let ((current-head (caar current-list)))
                    (cond ((null current-list) group-tag-to-append)
                          ((eq :startgrouptag current-head)
                           (iterate-in (cdr current-list) (1+ depth)))
                          ((eq :endgrouptag current-head)
                           (if (= 1 depth)
                               (-concat group-tag-to-append (cdr current-list))
                             (iterate-in (cdr current-list) (1- depth))))
                          (t (iterate-in (cdr current-list) depth))))))

      (iterate-pre original-list nil nil))))

;; Org roam
(after! org                        ; base org roam directory
  (setq org-roam-directory org-directory
        org-roam-dailies-directory "daily/"
        org-roam-mode-sections '(org-roam-backlinks-section
                                 org-roam-reflinks-section
                                 org-roam-unlinked-references-section))

  ;; Get `org-roam-preview-visit' and friends to replace the main window. This
  ;;should be applicable only when `org-roam-mode' buffer is displayed in a
  ;;side-window.
  ;; https://org-roam.discourse.group/t/sidebar-roam-buffer-gets-replaced-with-org-roam-node-instead-of-updating/2162/8
  (add-hook! 'org-roam-mode-hook
    (setq-local display-buffer--same-window-action
                '(display-buffer-use-some-window
                  (main)))))

(use-package! websocket
  :after org-roam)

(use-package! org-roam-ui
  :after org-roam ;; or :after org
  ;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
  ;;         a hookable mode anymore, you're advised to pick something yourself
  ;;         if you don't care about startup time, use
  ;;  :hook (after-init . org-roam-ui-mode)
  :config
  (setq org-roam-ui-sync-theme t
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t))

;; Org Agenda
(defun +paradoja/short-category ()
  (let ((cat (org-agenda-get-category)))
    (substring cat 0 (min (length cat) 11))))

(defun +paradoja/set-org-agenda-files ()
  "Sets the org agenda files"
  (interactive)
  (setq! org-agenda-files
         (mapcan (lambda (entry) (if (and (f-directory? entry))
                                     (cons entry
                                           (f-directories entry
                                                          (lambda (e)
                                                            (not (+paradoja/f-hidden? e 'in-any-part-of-the-path)))
                                                          'recursive))
                                   (list entry)))
                 (list org-directory))))

(after! org
  (map! :desc "Reload agenda files" :map doom-leader-notes-map
        "R" #'+paradoja/set-org-agenda-files)
  (+paradoja/set-org-agenda-files)

  (defun scheduling-string ()
    (let* ((scheduled
            (org-get-scheduled-time (point)))
           (deadline
            (org-get-deadline-time (point)))
           (char-to-add
            (cond ((and scheduled deadline) "ð")
                  (scheduled "s")
                  (deadline "d"))))
      (if char-to-add
          (format-time-string (format "%s[%%Y-%%m-%%d] " char-to-add) scheduled)
        "")))

  (setq! org-agenda-persistent-filter t
         org-agenda-skip-scheduled-if-done t
         org-agenda-skip-deadline-if-done t

         org-agenda-prefix-format
         '((agenda . " %i %-15:c%?-12t% s")
           (todo   . " %i %-15:c% l% s")
           (tags   . " %i %-15:c")
           (search . " %i %-15:c"))

         org-agenda-custom-commands
         `(("n" "Agenda and all TODOs"
            ((agenda "")
             (alltodo "")))
           ("w" "Work agenda & TODOs"
            ((tags-todo "+work"
                        ((org-agenda-prefix-format
                          " %i %-11:c% l% (scheduling-string)"))))))))

(use-package! org-super-agenda
  :after org-agenda
  :init
  (setq org-super-agenda-groups '((:name "Today"
                                   :time-grid t
                                   :scheduled today)
                                  (:name "Work"
                                   :tag "work")))

  :config
  (setq org-super-agenda-header-map (make-sparse-keymap))
  (org-super-agenda-mode 1))

;; Org capture && org roam capture
(after! org
  (let* ((logbook ":LOGBOOK:\nCREATED: %U\n:END:")
         (footer "%i\n%a")
         (entry-with-todo (format "* TODO %%?\n%s\n%s" logbook footer))
         (entry-without-todo (format  "* %%U %%?\n%s" footer)))
    (setq org-capture-templates
          `(("." "At point")
            (".-" "- Dated update" item
             (function (lambda () 'nothing))
             "- %?\n  UPDATE: %U")
            (".q" "- Dated question" item
             (function (lambda () 'nothing))
             "- Q:%?\n  UPDATE: %U\n  - ")

            ("P" "Personal")
            ("Pt" "Personal todo" entry
             (file+headline "todo.org" "Global Inbox")
             ,entry-with-todo :prepend t)
            ("Pn" "Personal notes" entry
             (file+headline "notes.org" "Global Notes Inbox")
             ,entry-without-todo :prepend t)

            ("p" "Project")
            ("pt" "Project-local todo" entry
             (file+headline ,#'+org-capture-project-todo-file "Project Inbox")
             ,entry-with-todo :prepend t)
            ("pn" "Project-local notes" entry
             (file+headline ,#'+org-capture-project-notes-file "Project Notes Inbox")
             ,entry-without-todo :prepend t)
            ("pc" "Project-local changelog" entry
             (file+headline ,#'+org-capture-project-changelog-file "Released")
             ,entry-without-todo :prepend t)

            ("C" "Centralized projects")
            ("Ct" "Central project todo" entry ,#'+org-capture-central-project-todo-file
             ,entry-with-todo :heading "Global Tasks" :prepend nil)
            ("Cn" "Central project notes" entry ,#'+org-capture-central-project-notes-file
             ,entry-without-todo :heading "Global Notes" :prepend t)
            ("Cc" "Central project changelog" entry ,#'+org-capture-central-project-changelog-file
             ,entry-without-todo :heading "Changelog" :prepend t))

          org-roam-capture-templates
          `(("d" "default" plain "%?"
             :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                                "#+title: ${title}\n")
             :unnarrowed t))

          org-roam-dailies-capture-templates
          `(("p" "personal" entry "* %<%H:%M> %?"
             :target (file+head "%<%Y-%m-%d>.org"
                                "#+title: %<%Y-%m-%d>\n"))))

    ;; copy to add in other projects as local var
    (setq +paradoja/org-capture-templates org-capture-templates
          +paradoja/org-roam-capture-templates org-roam-capture-templates
          +paradoja/org-roam-dailies-capture-templates org-roam-dailies-capture-templates)))

;; org-special-block-extras
(use-package! org-special-block-extras
  :after org
  :hook (org-mode . org-special-block-extras-mode))

(use-package! org-transclusion
  :after org
  :init
  (map!
   :map global-map "<f12>" #'org-transclusion-add
   :leader
   :prefix "n"
   :desc "Org Transclusion Mode" "t" #'org-transclusion-mode)
  (map! :localleader :map org-mode-map
        :prefix ("R" . "Org Transclusion")
        :desc "Open source" :nvi "o" #'org-transclusion-move-to-source
        :desc "Refresh" :nvi "r" #'org-transclusion-refresh))

;; org-pomodoro
(after! org
  (map! :desc "Pomodoro" :map doom-leader-notes-map
        "p" #'org-pomodoro))

;; anki-editor
(load! "anki-org.el")

;; Verb
(use-package! verb
  :after org
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((verb . t)
     (mermaid . t))))

(after! verb
  (map! :localleader :map org-mode-map
        :prefix ("v" . "Verb (HTTP)")
        :desc "Send & here" :nvi "h" #'verb-send-request-on-point
        :desc "Kill resp." :nvi "k" #'verb-kill-all-response-buffers
        :desc "Send & stay" :nvi "s" #'verb-send-request-on-point-other-window-stay
        :desc "Send & move" :nvi "o" #'verb-send-request-on-point-other-window
        :desc "Send & no win" :nvi "n" #'verb-send-request-on-point-no-window
        :desc "Set var" :nvi "S" #'verb-set-var
        :desc "Export" :nvi "E" #'verb-export-request-on-point
        (:prefix ("e" . "Export")
         :desc "Export (curl)" :nvi "c" #'verb-export-request-on-point-curl
         :desc "Export (eww)" :nvi "w" #'verb-export-request-on-point-eww
         :desc "Export (to verb)" :nvi "v" #'verb-export-request-on-point-verb)))

;; Open in...
(defun +org/open-external ()
  "Open a linked file (or actually, whatever) with xdg-open.

Useful when you don't want to open a PDF in Emacs."
  (interactive)
  (let* ((object (org-element-context))
         (path (and (eq (car object) 'link)
                    (org-element-property :path object))))
    (when path
      (message "%s" path)
      (call-process "xdg-open" nil nil nil path))))

(after! org
  (map! :localleader :map org-mode-map
        :desc "Open externally" :nvi "lo" #'+org/open-external))

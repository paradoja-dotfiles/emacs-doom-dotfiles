;;; org/anki-org.el -*- lexical-binding: t; -*-

(use-package! anki-editor
  :after org
  ;; Reset cloze-number after each capture.
  :hook (org-capture-after-finalize . anki-editor-reset-cloze-number)
  :config
  (setq anki-editor-org-tags-as-anki-tags t)
  (defvar paradoja/anki-editor-cloze-number 1))

(use-package! ankiorg
  :after anki-editor
  ;; :custom (ankiorg-media-directory "...")
  :commands
  ankiorg-pull-notes
  ankiorg-buffer-get-media-files
  ankiorg-pull-tags)

(use-package! anki-editor-ui
  :after anki-editor
  :bind (:map anki-editor-mode-map
              ("C-c e" . anki-editor-ui)))

(after! anki-editor
  (map! :localleader :map org-mode-map
        :prefix ("TAB" . "Anki")
        :desc "Cloze" :nvi "c" #'anki-editor-cloze-dwim
        :desc "Cloze inc" :nvi "i" #'anki-editor-cloze-region-auto-incr
        :desc "UI" :nvi "g" #'anki-editor-ui
        (:prefix ("C" . "Clozes")
         :desc "Cloze inc" :nvi "i" #'anki-editor-cloze-region-auto-incr
         :desc "Cloze no-inc" :nvi "n" #'anki-editor-cloze-region-dont-incr
         :desc "Cloze reset" :nvi "r" #'anki-editor-reset-cloze-number)
        :desc "Push notes" :nvi "p" #'anki-editor-push-notes
        :desc "Push tree"  :nvi "P" #'anki-editor-push-tree
        :desc "Push this" :nvi "." #'anki-editor-push-note-at-point))

(defun anki-editor-cloze-region-auto-incr (&optional arg)
  "Cloze region without hint and increase card number."
  (interactive)
  (anki-editor-cloze-region paradoja/anki-editor-cloze-number "")
  (setq paradoja/anki-editor-cloze-number (1+ paradoja/anki-editor-cloze-number))
  (forward-sexp))

(defun anki-editor-cloze-region-dont-incr (&optional arg)
  "Cloze region without hint using the previous card number."
  (interactive)
  (anki-editor-cloze-region (1- paradoja/anki-editor-cloze-number) "")
  (forward-sexp))

(defun anki-editor-reset-cloze-number (&optional arg)
  "Reset cloze number to ARG or 1"
  (interactive)
  (setq paradoja/anki-editor-cloze-number (or arg 1)))

(defun anki-editor-push-tree ()
  "Push all notes under a tree."
  (interactive)
  (anki-editor-push-notes 'tree)
  (anki-editor-reset-cloze-number))

;; Rewriting to change functionality
;; Basically how it's now:
;; - subheading-fields have priority over property fields (this is already in
;;   the original code)
;; - missing fields are sent with an empty string, instead of trying fill in or
;;   complain that they are not present
(defun anki-editor--map-fields (heading
                                content-before-subheading
                                subheading-fields
                                note-type
                                level
                                prepend-heading)
  "Map `heading', pre-subheading content, and subheadings to fields.

When the `subheading-fields' don't match the `note-type's fields,
map missing fields to the `heading' and/or `content-before-subheading'.
Return a list of cons of (FIELD-NAME . FIELD-CONTENT)."
  (anki-editor--with-collection-data-updated
    (let* ((model-fields (alist-get
                          note-type anki-editor--model-fields
                          nil nil #'string=))
           (property-fields (anki-editor--property-fields model-fields))
           (named-fields (seq-uniq (append subheading-fields property-fields)
                                   (lambda (left right)
                                     (string= (car left) (car right)))))
           (fields-matching (cl-intersection
                             model-fields (mapcar #'car named-fields)
                             :test #'string=))
           (fields-missing (cl-set-difference
                            model-fields (mapcar #'car named-fields)
                            :test #'string=))
           (fields-extra (cl-set-difference
                          (mapcar #'car named-fields) model-fields
                          :test #'string=))
           (fields (cl-loop for f in fields-matching
                            collect (cons f (alist-get
                                             f named-fields
                                             nil nil #'string=))))
           (heading-format anki-editor-prepend-heading-format))
      (cond ((equal 0 (length fields-missing))
             (when (< 0 (length fields-extra))
               (user-error "Failed to map all named fields")))
            (t                          ; with missing fields
             (let* ((fields-non-missing (mapcar (lambda (field) (cons field "")) fields-missing)))
               (append fields fields-non-missing))))
      fields)))

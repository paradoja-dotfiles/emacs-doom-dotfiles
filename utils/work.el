;;; utils/work.el -*- lexical-binding: t; -*-

(require 'f)

;; org capture templates and the rest are managed in org/config.el

;; (use-package! org-jira
;;   :after org)

;; (after! org-jira
;;   (let ((jira-config (f-join doom-private-dir
;;                              "local/work/jira.el")))
;;     (when (f-exists? jira-config)
;;       ;; (setq jiralib-url "https://company.atlassian.net"
;;       ;;       org-jira-working-dir "~/.org-jira")
;;       ;;   ...
;;       ;; (setq org-jira-custom-jqls
;;       ;;           '((:jql "
;;       ;;     project = \"PROJECT\"
;;       ;; AND issuetype in (Story, Task, Bug)
;;       ;; AND Sprint in openSprints()
;;       ;; AND NOT Sprint in futureSprints()
;;       ;; ORDER BY Rank DESC"
;;       ;;              ;; :limit 10
;;       ;;              :filename "current-sprint")))
;;       (load! jira-config)

;;       (map! :leader
;;             (:prefix ("oj" . "jira")
;;              :desc "issues" "j" #'org-jira-get-issues-from-custom-jql))
;;       (map! :map org-mode-map
;;             :leader
;;             (:prefix ("mj" . "jira")

;;              (:prefix ("p" . "projects")
;;               :desc "get" "pg" #'org-jira-get-projects)

;;              (:prefix ("i" . "issues")
;;               :desc "browse" "b" #'org-jira-browse-issue
;;               :desc "get (my)" "g" #'org-jira-get-issues
;;               :desc "get (custom)" "j" #'org-jira-get-issues-from-custom-jql
;;               :desc "get headonly" "h" #'org-jira-get-issues-headonly
;;               :desc "update" "u" #'org-jira-update-issue
;;               :desc "progress" "w" #'org-jira-progress-issue
;;               :desc "next (progress)" "n" #'org-jira-progress-issue-next
;;               :desc "assign" "a" #'org-jira-assign-issue
;;               :desc "refresh" "r" #'org-jira-refresh-issue
;;               :desc "Refresh in buffer" "R" #'org-jira-refresh-issues-in-buffer
;;               :desc "create" "c" #'org-jira-create-issue
;;               :desc "copy issue key" "k" #'org-jira-copy-current-issue-key
;;               :desc "get by fixversion" "f" #'org-jira-get-issues-by-fixversion)

;;              (:prefix ("s" . "subtask")
;;               :desc "create" "c" #'org-jira-create-subtask
;;               :desc "get" "g" #'org-jira-get-subtasks)

;;              (:prefix ("c" . "comment")
;;               :desc "add" "c" #'org-jira-add-comment
;;               :desc "update" "u" #'org-jira-update-comment)

;;              (:prefix ("w" . "worklogs")
;;               :desc "update" :"u" #'org-jira-update-worklogs-from-org-clocks)

;;              (:prefix ("t" . "todo")
;;               :desc "todo to jira" "tj" #'org-jira-todo-to-jira)))

;;       (when (and (bound-and-true-p org-jira-working-dir)
;;                  (not (f-dir? org-jira-working-dir)))
;;         (f-mkdir org-jira-working-dir)))))

;;; utils/paradoja.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Abby Henríquez Tejera
;;
;; Author: Abby Henríquez Tejera <https://github.com/paradoja>
;; Maintainer: Abby Henríquez Tejera <abby@blulaktuko.net>
;; Created: August 19, 2021
;; Modified: August 19, 2021
;; Version: 0.0.1
;; Keywords: utils
;;
;; Homepage: https.//gitlab.com/paradoja-dotfiles/emacs-doom-dotfiles.git
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary: Just an utils file
;;
;;  Description - Utils
;;
;;; Code:


;; http://ergoemacs.org/emacs/elisp_read_file_content.html
(defun get-string-from-file (filePath)
  "Return filePath's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))

(unless (fboundp 'file-name-concat) ; this exists apparently since v. 28.0.5 or so
  (defun file-name-concat (&rest parts)
    (cl-reduce (lambda (a b) (expand-file-name b a)) parts)))

(defun +paradoja/f-hidden? (path &optional consider-the-whole-path)
  "Return t if PATH is hidden, nil otherwise."
  (unless (f-exists? path)
    (error "Path does not exist: %s" path))
  (if consider-the-whole-path
      (-any? (lambda (s) (s-starts-with? "." s)) (f-split path))
    (s-starts-with? "." (car (last (f-split path))))))

(provide 'paradoja)
;;; paradoja.el ends here
